﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Toolbox.Extensions
{
    public static class Extensions
    {
        public static Vector3 PanToLookAt(this Transform thisTransform, Vector3 target, float distance = 10f)
        {
            // Get a unit vector pointing in the opposite direction as the transform (align w/ z, face negative)
            Vector3 desiredPosition = -(thisTransform.rotation * Vector3.forward);

            // Scale it to the desired length.
            desiredPosition *= distance;

            // Offset it to be relative to the target's position.
            desiredPosition += target;

            thisTransform.position = desiredPosition;

            return desiredPosition;
        }


        public static Vector3 Average(this ICollection<Vector3> pointCollection)
        {
            Vector3 average = new Vector3();
            foreach (Vector3 point in pointCollection)
            {
                average += point/pointCollection.Count;
            }
            return average;
        }
    }
}
