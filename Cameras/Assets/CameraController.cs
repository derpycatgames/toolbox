﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Toolbox.Extensions;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    public Transform TargetTransform;
    public float Distance = 10f;
    private Camera _thisCamera;


    void Start()
    {
        _thisCamera = GetComponent<Camera>();
    }


    void LateUpdate()
    {
        if (TargetTransform == null)
            return;

        //CenterViewOnTarget(TargetTransform.position, Distance);
        ContainInView(new [] {TargetTransform.position});
    }


    public void CenterViewOnTarget(Vector3 target, float distance)
    {
        transform.PanToLookAt(target, distance);
    }


    public void ContainInView(ICollection<Vector3> targets, float distance = 10f)
    {
        // Convert all targets to camera space
        Vector3[] targetArray = new Vector3[targets.Count];
        targets.CopyTo(targetArray, 0);

        for (int i = 0; i < targetArray.Length; i++)
        {
            targetArray[i] = _thisCamera.worldToCameraMatrix.MultiplyPoint3x4(targetArray[i]);
            Debug.Log(targetArray[i]);
        }

        // Find a set of bounds which will encapsulate all targets
        Bounds targetBounds = new Bounds(targetArray[0], Vector3.zero);

        foreach (Vector3 target in targetArray)
        {
            targetBounds.Encapsulate(target);
        }

        // Adjust the view to contain the bounds within the speficied margins
        if (_thisCamera.orthographic)
        {
            
        }
        else
        {
            
        }



    }
}
