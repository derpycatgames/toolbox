﻿using UnityEngine;
using System.Collections;

public class TileStructure
{
    public byte numTilesX = 4;
    public byte numTilesY = 4;
    public byte numTilesZ = 4;
    public bool[] gridCorners;




    public void GenerateGrid()
    {
        gridCorners = new bool[(numTilesX+1)*(numTilesY+1)*(numTilesZ+1)];
    }
}
